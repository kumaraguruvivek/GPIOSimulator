"""
Tester for GPIO GUI interaction using polling
"""

from RPiSim import GPIO
import time
import traceback

GPIO.set_verbosity(3)


def event_9(channel):
    GPIO.output(4, GPIO.HIGH)
    GPIO.output(17, GPIO.HIGH)


def event_25(channel):
    GPIO.output(4, GPIO.LOW)
    GPIO.output(17 ,GPIO.LOW)


def event_8(channel):
    GPIO.output(18, GPIO.HIGH)
    GPIO.output(21, GPIO.HIGH)


def event_11(channel):
    GPIO.output(18, GPIO.LOW)
    GPIO.output(21, GPIO.LOW)


def main():
    try:
        GPIO.setmode(GPIO.BCM)

        GPIO.setwarnings(False)

        GPIO.setup(4, GPIO.MODE_OUT)
        GPIO.setup(17, GPIO.MODE_OUT, initial=GPIO.HIGH)
        GPIO.setup(18, GPIO.MODE_OUT, initial=GPIO.LOW)
        GPIO.setup(21, GPIO.MODE_OUT, initial=GPIO.LOW)

        GPIO.setup(9, GPIO.MODE_IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(25, GPIO.MODE_IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(11, GPIO.MODE_IN)
        GPIO.setup(8, GPIO.MODE_IN, pull_up_down=GPIO.PUD_DOWN)

        GPIO.add_event_detect(9, GPIO.FALLING, callback=event_9)
        GPIO.add_event_detect(25, GPIO.RISING, callback=event_25)
        GPIO.add_event_detect(11, GPIO.RISING, callback=event_11)
        GPIO.add_event_detect(8, GPIO.RISING, callback=event_8)

        while True:
            time.sleep(100)

    except Exception:
        traceback.print_exc()
    finally:
        GPIO.cleanup()   # this ensures a clean exit


main()

