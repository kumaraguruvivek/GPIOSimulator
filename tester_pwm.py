"""
Tester for GPIO GUI interaction using polling
"""

from RPiSim import GPIO
import time
import traceback

GPIO.set_verbosity(3)


def main():
    try:
        GPIO.setmode(GPIO.BCM)

        GPIO.setwarnings(False)

        GPIO.setup(4, GPIO.MODE_OUT)
        p = GPIO.PWM(4, 100)
        time.sleep(1)
        while True:
            for dc in range(0, 101, 5):
                p.ChangeDutyCycle(dc)
                time.sleep(0.1)
            for dc in range(100, -1, -5):
                p.ChangeDutyCycle(dc)
                time.sleep(0.1)
            p.stop()
            time.sleep(1)

    finally:
        GPIO.cleanup()   # this ensures a clean exit


main()

